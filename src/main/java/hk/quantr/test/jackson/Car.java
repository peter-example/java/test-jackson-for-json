package hk.quantr.test.jackson;

/**
 *
 * @author peter
 */
public class Car {

	private String color;
	private String type;
	public byte[] bytes;

	public Car() {

	}

	public Car(String color, String type) {
		this.color = color;
		this.type = type;
	}

}
