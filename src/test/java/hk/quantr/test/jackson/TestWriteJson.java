package hk.quantr.test.jackson;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestWriteJson {

	@Test
	public void test() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);

		Car car = new Car("yellow", "renault");
		car.bytes = new byte[]{0x10, 0x20, 0x30};
//		objectMapper.writeValue(new File("car.json"), car);

		// pretty format
		String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(car);
		IOUtils.write(jsonString, new FileOutputStream("car.json"), "utf-8");
	}
}
