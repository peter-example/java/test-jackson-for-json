package hk.quantr.test.jackson;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestExtendJson {

	@Test
	public void test() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		String json = IOUtils.toString(new FileInputStream("car.json"), "utf-8");
		JsonNode jsonNode = objectMapper.readTree(json);
		((ObjectNode) jsonNode).putObject("address").put("loc", 123).put("att", 144);
		String color = jsonNode.get("color").asText();
		System.out.println(color);
		objectMapper.writeValue(new File("car.json"), jsonNode);
	}
}
