package hk.quantr.test.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestReadJson {

	@Test
	public void test() throws JsonProcessingException, FileNotFoundException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		String json = IOUtils.toString(new FileInputStream("car.json"), "utf-8");
		JsonNode jsonNode = objectMapper.readTree(json);
		String color = jsonNode.get("color").asText();
		System.out.println(jsonNode);
	}
}
